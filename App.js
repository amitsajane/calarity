/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component,useEffect } from 'react'
import {View,Text,StyleSheet,Image} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import {fcmService} from './src/Notification/FCMService ';
import {localNotificationService  } from './src/Notification/LocalNotificationService';





const App = () => {

  useEffect(()=>{
    fcmService.registerAppWithFCM();
    fcmService.register(onRegister, onNotification, onOpenNotification);
    localNotificationService.configure(onOpenNotification)
  },[])


  const onRegister = (token) => {
    console.log("[App] Token", token);
  }

  const onNotification = (notify) => {
    console.log("[App] onNotification", notify);
    const options = {
      soundName: 'default',
      playSound: true,
    }

    localNotificationService.showNotification(
      0,
      notify.notification.title,
      notify.notification.body,
      notify,
      options,
    )
  }

  const onOpenNotification = async (notify) => {  
    console.log('notify', notify);
  }




  return (
    <View style={styles.container}>
      <Text>PushNotification With Firebase</Text>
        <Image 
        style={{width:200,height:200}}
        source={{uri:'https://cdn-media-1.freecodecamp.org/images/0*CPTNvq87xG-sUGdx.png'}}
        />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});


export default App;

